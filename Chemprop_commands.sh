#!/bin/bash

# Base directories
base_output_dir="/cluster/home/oliveraz/hyperparameter-seed3/output"
data_dir="/cluster/home/oliveraz/hyperparameter-seed3/data"
template_file="job_template.sh"

# Ensure the output directory exists
mkdir -p "$base_output_dir"

# Define hyperparameter combinations
hidden_sizes=(300 500)
depths=(3 5)
dropouts=(0.0 0.2)

# Create a template job script
cat << 'EOF' > $template_file
#!/bin/bash
#SBATCH --job-name=chemprop_train_{hidden_size}_{depth}_{dropout}_run_{fold_index}
#SBATCH --output=chemprop_train_{hidden_size}_{depth}_{dropout}_run_{fold_index}_%j.out
#SBATCH --error=chemprop_train_{hidden_size}_{depth}_{dropout}_run_{fold_index}_%j.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=24:00:00
#SBATCH --partition=normal

# Load necessary modules
module load gcc/6.3.0 python/3.8.5

# Activate virtual environment
source ~/chemprop_env/bin/activate

# Ensure the save directory exists
save_dir={save_dir}
mkdir -p "$save_dir"

# Train and validate the model
chemprop_train \
  --data_path {train_path} \
  --dataset_type regression \
  --save_dir "$save_dir" \
  --metric r2 \
  --smiles_column Smiles \
  --target_columns pIC50 \
  --seed 3 \
  --split_type predetermined \
  --folds_file {folds_file} \
  --test_fold_index {fold_index} \
  --separate_val_path {val_path} \
  --separate_test_path {test_path} \
  --hidden_size {hidden_size} \
  --depth {depth} \
  --dropout {dropout} \
  --num_workers $SLURM_CPUS_PER_TASK
EOF

# Loop through hyperparameter combinations and splits
for hidden_size in "${hidden_sizes[@]}"
do
  for depth in "${depths[@]}"
  do
    for dropout in "${dropouts[@]}"
    do
      for i in {0..2}
      do
        # Define the save directory for this run
        save_dir="${base_output_dir}/save_dir_${hidden_size}_${depth}_${dropout}_run_$i"

        # Set the data paths based on the fold index
        train_path="${data_dir}/train_fold_${i}.csv"
        val_path="${data_dir}/val_fold_${i}.csv"
        test_path="${data_dir}/test_fold_${i}.csv"
        folds_file="${data_dir}/folds_file.csv"

        # Verify data paths
        if [ ! -f "$train_path" ]; then
          echo "Missing train file: $train_path"
          continue
        fi

        if [ ! -f "$val_path" ]; then
          echo "Missing validation file: $val_path"
          continue
        fi

        if [ ! -f "$test_path" ]; then
          echo "Missing test file: $test_path"
          continue
        fi

        # Create a job script for this set of hyperparameters and split index
        job_script="job_${hidden_size}_${depth}_${dropout}_run_$i.sh"
        sed -e "s/{hidden_size}/$hidden_size/g" \
            -e "s/{depth}/$depth/g" \
            -e "s/{dropout}/$dropout/g" \
            -e "s#{save_dir}#$save_dir#g" \
            -e "s#{train_path}#$train_path#g" \
            -e "s#{val_path}#$val_path#g" \
            -e "s#{test_path}#$test_path#g" \
            -e "s#{folds_file}#$folds_file#g" \
            -e "s/{fold_index}/$i/g" \
            $template_file > $job_script

        # Submit the job script
        sbatch $job_script
      done
    done
  done
done

# Clean up the template file
rm $template_file
