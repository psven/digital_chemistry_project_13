# Digital_Chemistry_project_13

## Description
The prediction of pIC50 values for MCF7 breast cancer cells was attempted. We used a large ChEMBL data set (ChEMBL387) and trained several models which predict pIC50 values based on smiles strings. The code files are split into the different model types. Separate files are provided for data curation, PCA/clustering analysis and y-scrambling:

1. CHEMBL387_IC50.csv - Original,unprocessed data set which was downloaded from the ChEMBL database on 30.03.2024
2. allassays_nodupl_nofrag.csv - Processed dataset after data curation
3. Data_Curation_ChEMBL.ipynb - Code for data curation
4. NeuralNetwork.ipynb - Training and testing of feedforward neural network based on Keras
5. XGBoost.ipynb - Training and testing of XGBoost model based on scikit-learn
6. RandomForest.ipynb - Training and testing of random forest model based on scikit-learn
7. SupportVectorRegressor.ipynb - Training and testing of SVM model based on scikit-learn
8. Chemprop_MPNN.ipynb - Training and testing of MPNN model
9. Chemprop_commands.sh - Bash commands for hyperparameter grid search using the ETH Euler cluster
10. Chemprop_summary_test_scores.csv - Scores for testing of the MPNN model
11. NeuralNetwork_Keras_Y_Scrambling.ipynb - Y-scrambling test for the feedforward neural network based on Keras
12. PCA_clustering.ipynb - PCA analysis and clustering based on scikit-learn KMeans function and visual inspection


## Environment
Our models have been trained and tested using the Google Colab Environment. The hyperparameter optimization for the MPNN model using Chemprop was done on the ETH Euler Cluster.

